# Realtime Chat [Showcase]

This should be a demo for presenting our work. The application is composed of a WebSocket server written in Golang and a ReactJS frontend with NextJS written in TypeScript, using Tailwind CSS. The application was developed as Docker containers and is ready to be deployed into a Kubernetes cluster.

The system should save the data in a Redis database; however, since this is just a showcase, the feature wasn't implemented. If you need more information, don't hesitate to reach out to us at helmer@barcos.co

<br>
<div align="center" justify="center">
<img src="realtime-chat.png" align="center" width="100%" alt="Realtime Chat">
</div>
<br>

# Technology Stack

- Golang for the backend and WebSocket server
- NextJS for the frontend. The frontend is a ReactJS application written in TypeScript
- Tailwind for CSS and Shadcn as the UI library.
- Docker for creating containers
- k3s for deploying the current live demo
