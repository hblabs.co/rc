package handlers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/hblabs.co/rc/realtime/models"
)

func HealthCheck(c *fiber.Ctx) error {
	return c.JSON(models.HealthCheck{Status: "running"})
}
