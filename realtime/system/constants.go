package system

const REALTIME_PORT_KEY = "REALTIME_PORT"

const (
	HealthCheckRoute = "/healthz"
	WebsocektRoute   = "/ws"
)
