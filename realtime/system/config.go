package system

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

var appConfig = AppConfigs{}

type AppConfigs struct {
	Port int
}

func ReadAppConfigs() {
	appConfig.readPort()
}

func GetAppConfigs() AppConfigs {
	return appConfig
}

func (app *AppConfigs) readPort() {
	app.Port = getPort()
}

func getPort() int {
	value := os.Getenv(REALTIME_PORT_KEY)
	port, err := strconv.Atoi(value)
	if err != nil {
		panic(err)
	}

	if port <= 0 {
		panic(errors.New(fmt.Sprintf("port %v is not a valid port", port)))
	}

	return port
}
