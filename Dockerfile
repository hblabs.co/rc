####### Realtime Server
FROM golang:1.21 as rcrealtime
LABEL maintainer="Helmer Barcos helmer@barcos.co"
ENV WORKDIR=/hblabs.co/rc/realtime
WORKDIR $WORKDIR
COPY realtime/go.mod realtime/go.sum ./
COPY go.work ../
RUN go install github.com/cosmtrek/air@latest
RUN go install github.com/go-delve/delve/cmd/dlv@latest
RUN go mod download && go mod verify
CMD air -c .air.toml

####### Realtime Server
FROM rcrealtime as rcrealtime_production_base
LABEL maintainer="Helmer Barcos helmer@barcos.co"
COPY realtime .
RUN CGO_ENABLED=0 go build -o main main.go

FROM alpine:latest as rcrealtime_production
ENV WORKDIR=/hblabs.co/rc/realtime
WORKDIR $WORKDIR
COPY --from=rcrealtime_production_base /hblabs.co/rc/realtime/main main
EXPOSE 3030
CMD /hblabs.co/rc/realtime/main

#### Webclient
FROM node:20 as rcwebclient_development
LABEL maintainer="Helmer Barcos - helmer@barcos.co"
ENV WORKDIR=/hblabs.co/rc/webclient
WORKDIR $WORKDIR
RUN apt update
RUN npm install -g npm@10.2.1
COPY webclient/package*.json webclient/tsconfig.json $WORKDIR/
COPY webclient $WORKDIR
RUN npm i
CMD npm run dev

#### Webclient
FROM node:20 as rcwebclient_production_base
LABEL maintainer="Helmer Barcos - helmer@barcos.co"
ENV WORKDIR=/hblabs.co/rc/webclient
WORKDIR $WORKDIR
RUN apt update
RUN npm install -g npm@10.2.1
COPY webclient/package*.json webclient/tsconfig.json $WORKDIR/
COPY webclient $WORKDIR
RUN npm i
RUN npm run build


FROM node:20 as rcwebclient_production
LABEL maintainer="Helmer Barcos - helmer@barcos.co"
ENV WORKDIR=/hblabs.co/rc/webclient
WORKDIR $WORKDIR
COPY --from=rcwebclient_production_base /hblabs.co/rc/webclient/package.json package.json
COPY --from=rcwebclient_production_base /hblabs.co/rc/webclient/next.config.mjs next.config.mjs
COPY --from=rcwebclient_production_base /hblabs.co/rc/webclient/node_modules /hblabs.co/rc/webclient/node_modules
COPY --from=rcwebclient_production_base /hblabs.co/rc/webclient/.next /hblabs.co/rc/webclient/.next
COPY --from=rcwebclient_production_base /hblabs.co/rc/webclient/public /hblabs.co/rc/webclient/public
EXPOSE 3000
CMD cd /hblabs.co/rc/webclient && npm run start