import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { ScrollArea } from "@/components/ui/scroll-area";
import { Textarea } from "@/components/ui/textarea";
import { set } from "date-fns";
import { MouseEvent, useCallback, useEffect, useRef, useState } from "react";

type ReailtimeParams = {
  realtimePort: string;
  realtimeHost: string;
  realtimeProtocol: string;
};

type WebhookMessage = {
  colorIndex: string;
  session: string;
  message: string;
};

const colorNames = [
  "slate",
  "red",
  "orange",
  "amber",
  "yellow",
  "lime",
  "green",
  "emerald",
  "teal",
  "cyan",
  "sky",
  "blue",
  "indigo",
  "violet",
  "purple",
  "fuchsia",
  "pink",
  "rose",
];
function getRandomElement(arr: (string | number)[]) {
  const randomIndex = Math.floor(Math.random() * arr.length);
  const selection = arr[randomIndex];
  return { selection, index: randomIndex };
}

const colorVariants = [
  "bg-slate-200",
  "bg-red-200",
  "bg-orange-200",
  "bg-amber-200",
  "bg-yellow-200",
  "bg-lime-200",
  "bg-green-200",
  "bg-emerald-200",
  "bg-teal-200",
  "bg-cyan-200",
  "bg-sky-200",
  "bg-blue-200",
  "bg-indigo-200",
  "bg-violet-200",
  "bg-purple-200",
  "bg-fuchsia-200",
  "bg-pink-200",
  "bg-rose-200",
];

export const Chat = () => {
  const [bgColor, setBgColor] = useState("");
  const [bgColorIndex, setBgColorIndex] = useState(0);

  const [session, setSession] = useState("");
  const [messages, setMessages] = useState<WebhookMessage[]>([]);
  const [realtime, setRealtime] = useState({ port: "", host: "" });
  const [socket, setSocket] = useState<WebSocket | undefined>(undefined);
  const textAreaRef = useRef<HTMLTextAreaElement>(null);

  const fetchRealtimeParams = useCallback(async () => {
    const url = "/api/params";
    const res = await fetch(url);
    if (!res.ok) throw new Error(`Request to ${url} failed`);
    const json = res.json();
    return json as unknown as ReailtimeParams;
  }, []);

  useEffect(() => {
    setSession(crypto.randomUUID());
  }, []);

  useEffect(() => {
    if (session && !bgColor) {
      const { selection, index } = getRandomElement(colorVariants);
      setBgColor(selection as string);
      setBgColorIndex(index);
    }
  }, [bgColor, session]);

  useEffect(() => {
    fetchRealtimeParams()
      .then(({ realtimeHost: host, realtimePort: port, realtimeProtocol }) => {
        const webSocket = new WebSocket(
          `${realtimeProtocol}://${host}:${port}/ws`
        );
        // const webSocket = new WebSocket(`ws://localhost:${port}/ws`);
        webSocket.onmessage = (evt) => {
          try {
            const { message, session, colorIndex } = JSON.parse(evt.data);
            setMessages((prev) => [...prev, { message, session, colorIndex }]);
          } catch (err) {
            console.error(err);
          }
        };

        setRealtime({ port, host });
        setSocket(webSocket);
      })
      .catch(console.error);
  }, [fetchRealtimeParams]);

  const sendMessage = () => {
    const value = textAreaRef.current?.value;
    if (!value || !socket) return;
    socket.send(
      JSON.stringify({ colorIndex: bgColorIndex, session, message: value })
    );
    textAreaRef.current.value = "";
  };

  return (
    <div className="grid h-full w-full bg-background">
      <ScrollArea>
        <div className="grid w-full h-full">
          {messages?.map(
            ({ colorIndex, message, session: messageSession }, index) => (
              <div
                // style={{ width: messageSession === session ? }}
                style={{
                  width: "fit-content",
                  minWidth: "250px",
                  justifySelf:
                    messageSession === session ? "flex-end" : "flex-start",
                }}
                key={`message-${index}`}
                className={`relative my-1 rounded-lg mx-1 px-2 py-1 ${
                  colorVariants[Number(colorIndex)]
                }`}
              >
                <p className="text-sm mb-5 mx-2">{message}</p>
                <Badge
                  variant="secondary"
                  className="text-[8px] absolute bottom-1"
                >
                  {session}
                </Badge>
              </div>
            )
          )}
        </div>
      </ScrollArea>
      <div className={`mt-auto ${bgColor}`}>
        <div className="px-4 my-2">
          <Textarea
            ref={textAreaRef}
            placeholder="Send a message to the room. (open this website in another browser tab to chat in real time)"
          />
        </div>

        <div className="w-full flex px-4 pb-2 items-end justify-center">
          <div className="flex flex-col">
            <Label className="text-md w-[300px]">Session: {session}</Label>
          </div>

          <Button className="ml-auto mr w-48 h-12" onClick={sendMessage}>
            Send
          </Button>
        </div>
      </div>
    </div>
  );
};
