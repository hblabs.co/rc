// defaults to auto
export const dynamic = 'force-dynamic';

export function GET() {
	return Response.json({ status: 'running' });
}
