import { unstable_noStore as noStore } from "next/cache";

export const dynamic = "force-dynamic";
export function GET() {
  noStore();
  return Response.json({
    realtimePort: process.env.REALTIME_PORT,
    realtimeHost: process.env.REALTIME_HOST,
    realtimeProtocol: process.env.NODE_ENV === "production" ? "wss" : "ws",
  });
}
